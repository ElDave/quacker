﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Quacker.Models
{
    public class UserViewQuacks
    {
        public List<Quack> NewQuacks { get; set; }
        public List<Quack> RandomQuacks { get; set; }

    }
}