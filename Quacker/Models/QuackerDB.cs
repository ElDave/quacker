﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Quacker.Models
{
    public class QuackerDB: DbContext
    {
        public DbSet<User> UserTable { get; set; }
        public DbSet<Quack> QuackTable { get; set; }
    }
}