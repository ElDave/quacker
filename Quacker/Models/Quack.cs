﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Quacker.Models
{
    public class Quack
    {
        public int QuackId { get; set; }

        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }

        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Quack Text is required")]
        public string Text { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

    }
}