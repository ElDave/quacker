namespace Quacker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hello : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Quacks", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Quacks", "Text", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Quacks", "Text", c => c.String());
            AlterColumn("dbo.Quacks", "Title", c => c.String());
        }
    }
}
