namespace Quacker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class quacks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Quacks",
                c => new
                    {
                        QuackId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Date = c.DateTime(nullable: false),
                        Text = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuackId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Quacks", "UserId", "dbo.Users");
            DropIndex("dbo.Quacks", new[] { "UserId" });
            DropTable("dbo.Quacks");
        }
    }
}
