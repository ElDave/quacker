﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Quacker.Models;

namespace Quacker.Controllers
{
    public class UsersController : Controller
    {
        private QuackerDB db = new QuackerDB();

        protected bool IsUnique(User user)//  check if username and email are unique 
        {
            var username = db.UserTable.SingleOrDefault(a => a.UserName == user.UserName);
            var email = db.UserTable.SingleOrDefault(a => a.Email == user.Email);
            if (username == null && email == null) return true;
            else return false;

        }

       [HttpGet]
       public ActionResult ViewUsers()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            var users = db.UserTable;
            return View(users.ToList());
        }
        // GET: Users/Create
        public ActionResult Register()
        {
            return View();
        }

        // POST: Users/Register
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "UserId,FirstName,LastName,UserName,Email,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                if (IsUnique(user))
                {
                    db.UserTable.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("Login");
                }
                else ViewBag.Message = "User Name Or Email already in use";
            }

            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login([Bind(Include = "UserName,Password")] User user)
        {
            var DBUser = db.UserTable.SingleOrDefault(u => u.UserName == user.UserName && u.Password == user.Password);
            if (DBUser != null)
            {
                TempData["LoggedIn"] = DBUser.UserName + " logged in Successfully";
                Session["UserID"] = DBUser.UserId;
                Session["UserName"] = DBUser.UserName;
                return RedirectToAction("Index", "Home");
            }
            else ModelState.AddModelError("", "Incorrect User Name and password combination");
            return View();
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session["UserID"] = null;
            Session["UserName"] = null;
            return RedirectToAction("Index", "Home");
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.UserTable.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UsedId,FirstName,LastName,UserName,Email,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.UserTable.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.UserTable.Find(id);
            db.UserTable.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
