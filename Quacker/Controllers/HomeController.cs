﻿using Quacker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Quacker.Controllers
{
    public class HomeController : Controller
    {
        private QuackerDB db = new QuackerDB();

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Landing", "Home");
            }
            var quackList = (from a in db.QuackTable where a.QuackId != null orderby a.Date descending select a);
            var NewQuacks = quackList.Take(3).ToList();
            Random random = new Random();
            

            var RandomQuacks = quackList.Skip(random.Next(0, db.QuackTable.Count() - 5)).Take(5).ToList();
            var quacks = new UserViewQuacks();
            quacks.NewQuacks = NewQuacks;
            quacks.RandomQuacks = RandomQuacks;
            return View(quacks);
        }
        public ActionResult Landing()
        {
            var quacks = db.QuackTable.ToList();
            Random random = new Random();
            Quack quack = quacks.ElementAt(random.Next(0, quacks.Count()));
            return View(quack);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}