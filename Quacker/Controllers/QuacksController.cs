﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Quacker.Models;

namespace Quacker.Controllers
{
    public class QuacksController : Controller
    {
        private QuackerDB db = new QuackerDB();

        // GET: Quacks
        public ActionResult Index()
        {
            var quackTable = db.QuackTable.Include(q => q.User);
            return View(quackTable.ToList());
        }

        public ActionResult ViewQuacks()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            var id = Session["UserID"] as int?;
            var quackTable = db.QuackTable.Where(quack => quack.UserId == id);
            return View(quackTable.ToList());
        }
        // GET: Quacks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quack quack = db.QuackTable.Include(q => q.User).SingleOrDefault(q => q.QuackId == id);
            if (quack == null)
            {
                return HttpNotFound();
            }
            return View(quack);
        }

        // GET: Quacks/Create
        public ActionResult Create()
        {
            if (Session["UserID"] == null) {
                return RedirectToAction("Login", "Users");
            }
            return View();
        }

        // POST: Quacks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QuackId,Title,Text")] Quack quack)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            quack.UserId = Session["UserID"] as int? ?? default(int);
            quack.Date = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.QuackTable.Add(quack);
                db.SaveChanges();
                return RedirectToAction("ViewQuacks");
            }
            return View(quack);
        }

        // GET: Quacks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quack quack = db.QuackTable.Find(id);
            if (quack == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.UserTable, "UserId", "FirstName", quack.UserId);
            return View(quack);
        }

        // POST: Quacks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "QuackId,Title,Text")] Quack quack)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (ModelState.IsValid)
            {
                var modifiedQuack = db.QuackTable.Find(quack.QuackId);
                modifiedQuack.Title = quack.Title;
                modifiedQuack.Text = quack.Text;
                db.Entry(modifiedQuack).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.UserTable, "UserId", "FirstName", quack.UserId);
            return View(quack);
        }

        // GET: Quacks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quack quack = db.QuackTable.Find(id);
            if (quack == null)
            {
                return HttpNotFound();
            }
            return View(quack);
        }

        // POST: Quacks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            Quack quack = db.QuackTable.Find(id);
            db.QuackTable.Remove(quack);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
